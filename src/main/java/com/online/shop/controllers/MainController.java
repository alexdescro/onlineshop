package com.online.shop.controllers;

import com.online.shop.dtos.*;
import com.online.shop.services.CustomerOrderService;
import com.online.shop.services.ProductService;
import com.online.shop.services.ShoppingCartService;
import com.online.shop.services.UserService;
import com.online.shop.validators.ChosenProductValidator;
import com.online.shop.validators.ProductValidator;
import com.online.shop.validators.UserValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

@Controller
public class MainController {

      @Autowired
      private UserService userService;

      @Autowired
      private ProductService productService;
      @Autowired
      private ShoppingCartService shoppingCartService;
      @Autowired
      private CustomerOrderService customerorderService;

      @Autowired
      private UserValidator userValidator;

      @Autowired
      private ProductValidator productValidator;

      @Autowired
      private ChosenProductValidator chosenProductValidator;

      // metoda de mai jos - addProductPageGet() - este doar cu titlu de exemplu...
      // cea mai suplă implementare a acestei condiționalități este realizată în SecurityConfig ...
      // cu ...    .antMatchers("/addProduct").hasRole("ADMIN")
      @GetMapping("/addProduct")
      public String addProductPageGet(Model model,
                                      @RequestParam(value = "productAddedSuccessfully", required = false) Boolean productAddedSuccessfully) {
            menuPageGet(model);
            if (userService.userIsAdmin()) {
                  ProductDto productDto = new ProductDto();
                  model.addAttribute("productDto", productDto);
                  if (productAddedSuccessfully != null && productAddedSuccessfully) {
                        model.addAttribute("message444", "Product was added successfully in the shop offer!");
                  }
                  return "addProduct";
            } else {
                  return "redirect:/home";
            }
      }

      @PostMapping("/addProduct")
      public String addProductPagePost(@ModelAttribute ProductDto productDto, BindingResult bindingResult,
                                       @RequestParam("productImage") MultipartFile multipartFile, Model model,
                                       RedirectAttributes redirectAttributes) throws IOException {
            productValidator.validate(productDto, bindingResult);
            if (bindingResult.hasErrors()) {
                  menuPageGet(model);
                  return "addProduct";  // fără acel redirect:/ (de mai jos) întoarce aceeași pagină cu câmpurile
                  // completate așa cum le-a băgat utilizatorul
            }
            productService.addProduct(productDto, multipartFile);
            redirectAttributes.addAttribute("productAddedSuccessfully", true);
            return "redirect:/addProduct"; // cu acest redirect:/ întoarce aceeași pagină cu câmpurile goale
      }

      @GetMapping("/home")
      public String homepageGet(Model model) {
            menuPageGet(model);
            List<ProductDto> productDtoList = productService.getAllProductDtos();
            model.addAttribute("productDtoList", productDtoList);
            System.out.println(productDtoList);
            return "homepage";
      }

      @GetMapping("/register")
      public String registerPageGet(Model model,
                                    @RequestParam(value = "userAddedSuccessfully", required = false) Boolean userAddedSuccessfully) {
            System.out.println("Am accesat pagina de reghe-register!");
            System.out.println("A fost inregistrat userul introdus? ... " + userAddedSuccessfully);
            UserDto userDto = new UserDto();
            model.addAttribute("userDto", userDto);
            if (userAddedSuccessfully != null && userAddedSuccessfully) {
                  model.addAttribute("message888", "User was added successfully!");
            }
            return "register";
      }

      @PostMapping("/register")
      public String registerPagePost(@ModelAttribute UserDto userDto, BindingResult bindingResult,
                                     RedirectAttributes redirectAttributes) {
            userValidator.validate(userDto, bindingResult);
            if (bindingResult.hasErrors()) {
                  return "register";
            }
            userService.addUser(userDto);

            redirectAttributes.addAttribute("userAddedSuccessfully", true);
            return "redirect:/register";
      }

      @GetMapping("/login")
      public String loginPageGet(Model model, @ModelAttribute("userAddedSuccessfully") String userAddedSuccessfully) {
            System.out.println("Am intrat prin pagina de easy-login la pagina căutată (după autentificare)!");
            model.addAttribute("userAddedSuccessfully", userAddedSuccessfully);
            return "login";
      }

      @GetMapping("/loginError")
      public String loginErrorPageGet() {
            return "loginError";
      }

      @GetMapping("/product/{productId}")
      public String viewProductGet(@PathVariable(value = "productId") String productId, Model model) {
            menuPageGet(model);
            Optional<ProductDto> optionalProductDto = productService.getOptionalProductDtoById(productId);
            if (optionalProductDto.isEmpty()) {
                  return "productError";
            }
            ProductDto productDto = optionalProductDto.get();
            model.addAttribute("productDto", productDto);
            System.out.println("Am dat click pe produsul cu ID-ul " + productId);
            ChosenProductDto chosenProductDto = new ChosenProductDto();
            model.addAttribute("chosenProductDto", chosenProductDto);
            return "viewProduct";
      }

      @PostMapping("/product/{productId}")
      public String viewProductPost(@PathVariable(value = "productId") String productId, Model model,
                                    @ModelAttribute("chosenProductDto") ChosenProductDto chosenProductDto,
                                    BindingResult bindingResult) {
            menuPageGet(model);
            if (userService.userIsAdmin()) {
                  chosenProductValidator.validateStockAdjustment(chosenProductDto, productId, bindingResult);
                  if (bindingResult.hasErrors()) {
                        Optional<ProductDto> optionalProductDto = productService.getOptionalProductDtoById(productId);
                        ProductDto productDto = optionalProductDto.get();
                        model.addAttribute("productDto", productDto);
                        return "viewProduct";
                  }
                  productService.adjustStock(chosenProductDto, productId);
            } else {
                  chosenProductValidator.validate(chosenProductDto, productId, bindingResult);
                  if (bindingResult.hasErrors()) {
                        Optional<ProductDto> optionalProductDto = productService.getOptionalProductDtoById(productId);
                        ProductDto productDto = optionalProductDto.get();
                        model.addAttribute("productDto", productDto);
                        return "viewProduct";
                  }
                  String loggedInUserEmail = SecurityContextHolder.getContext().getAuthentication().getName();
                  shoppingCartService.addToCart(chosenProductDto, productId, loggedInUserEmail);
            }
            return "redirect:/product/" + productId;
      }

      @PostMapping("/product/newPrice/{productId}")
      public String newProductPricePost(@PathVariable(value = "productId") String productId, Model model,
                                        @ModelAttribute("chosenProductDto") ChosenProductDto chosenProductDto,
                                        BindingResult bindingResult) {
            menuPageGet(model);
            if (userService.userIsAdmin()) {
                  chosenProductValidator.validatePriceAdjustment(chosenProductDto, productId, bindingResult);
                  if (bindingResult.hasErrors()) {
                        Optional<ProductDto> optionalProductDto = productService.getOptionalProductDtoById(productId);
                        ProductDto productDto = optionalProductDto.get();
                        model.addAttribute("productDto", productDto);
                        return "viewProduct";
                  }
                  productService.adjustSellingPrice(chosenProductDto, productId);
            }
            return "redirect:/product/" + productId;
      }

      @GetMapping("/cart")
      public String cartPageGet(Model model) {
            menuPageGet(model);
            String loggedInUserEmail = SecurityContextHolder.getContext().getAuthentication().getName();
            ShoppingCartDto shoppingCartDto = shoppingCartService.getShoppingCartDtoByUserEmail(loggedInUserEmail);
            model.addAttribute("shoppingCartDto", shoppingCartDto);
            return "cart";
      }

      @PostMapping("/cart/remove/{cartItemId}")
      public String cartRemoveItem(@PathVariable(value = "cartItemId") String cartItemId) {
            shoppingCartService.removeShoppingCartItem(cartItemId);
            return "redirect:/cart";
      }

      @PostMapping("/cart/increaseQtyByOne/{cartItemId}")
      public String cartIncreaseItemQtyByOne(@PathVariable(value = "cartItemId") String cartItemId) {
            shoppingCartService.increaseItemQtyByOne(cartItemId);
            return "redirect:/cart";
      }

      @PostMapping("/cart/decreaseQtyByOne/{cartItemId}")
      public String cartDecreaseItemQtyByOne(@PathVariable(value = "cartItemId") String cartItemId) {
            shoppingCartService.decreaseItemQtyByOne(cartItemId);
            return "redirect:/cart";
      }

      @GetMapping("/checkout")
      public String checkoutPageGet(Model model) {
            menuPageGet(model);
            String loggedInUserEmail = SecurityContextHolder.getContext().getAuthentication().getName();

            ShoppingCartDto shoppingCartDto = shoppingCartService.getShoppingCartDtoByUserEmail(loggedInUserEmail);
            model.addAttribute("shoppingCartDto", shoppingCartDto);

            UserDetailsDto userDetailsDto = userService.getUserDetailsDtoByEmail(loggedInUserEmail);
            model.addAttribute("userDetailsDto", userDetailsDto);

            return "checkout";
      }

      @PostMapping("/sendOrder")
      public String sendOrderPagePost(@ModelAttribute UserDetailsDto userDetailsDto) {
            String loggedInUserEmail = SecurityContextHolder.getContext().getAuthentication().getName();
            customerorderService.addCustomerOrder(loggedInUserEmail, userDetailsDto);
            return "confirmation";
      }

      @GetMapping("/ordersTracking")
      public String ordersTrackingPageGet(Model model) {
            menuPageGet(model);
//        List<CustomerOrderDto> customerOrderDtoList = new ArrayList<>();
            List<CustomerOrderDto> customerOrderDtoList = customerorderService.getAllCustomerOrdersDtos();
            model.addAttribute("customerOrderDtoList222", customerOrderDtoList);
            return "ordersTracking";
      }

      @GetMapping("/myOrdersTracking")
      public String myOrdersTrackingPageGet(Model model) {
            menuPageGet(model);
            String loggedInUserEmail = SecurityContextHolder.getContext().getAuthentication().getName();
            List<CustomerOrderDto> customerOrderDtoList = customerorderService.getCustomerOrdersDtosByUserEmail(loggedInUserEmail);
            model.addAttribute("customerOrderDtoList777", customerOrderDtoList);
            return "myOrdersTracking";
      }

      private void menuPageGet(Model model) {
            String loggedInUserEmail = SecurityContextHolder.getContext().getAuthentication().getName();

            UserDto userDto = userService.getUserDtoByEmail(loggedInUserEmail);
            model.addAttribute("userDto91", userDto);

            ShoppingCartDto shoppingCartDto = shoppingCartService.getShoppingCartDtoByUserEmail(loggedInUserEmail);
            model.addAttribute("shoppingCartDtoYY88", shoppingCartDto);
      }

      @GetMapping("/search")
      public String searchPageGet(Model model, @RequestParam(name = "searchTerm", required = false) String query) {
            menuPageGet(model);
            List<ProductDto> productDtoList = productService.getSearchedProductsDtos(query);
            model.addAttribute("productDtoList1414", productDtoList);
            return "search";
      }

}
