package com.online.shop.dtos;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ChosenProductDto {
    private String quantity;
    private String price;
    private ProductDto productDto;
}
