package com.online.shop.dtos;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@ToString
public class ShoppingCartDto {

    private List<ShoppingCartItemDto> items;
    private String subTotal;
    private String total;
    private String totalNumberOfProducts;

    public ShoppingCartDto() {
        items = new ArrayList<>(); // aici am declarat un constructor în care am inițializat variabila items
    }

    public void add(ShoppingCartItemDto shoppingCartItemDto) {
        items.add(shoppingCartItemDto);
    }
}
