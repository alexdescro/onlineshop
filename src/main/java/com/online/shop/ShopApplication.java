package com.online.shop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@SpringBootApplication
@RestController
public class ShopApplication {

    public static void main(String[] args) {
        SpringApplication.run(ShopApplication.class, args);
    }

    record Person(String name, int age, double savings) {
    }

    record GreetResponse(String greet, List<String> favProgrammingLanguages, Person person) {
    }

    @GetMapping("/greet")
    public GreetResponse greet() {
        GreetResponse response = new GreetResponse(
                "Hello to me!",
                List.of("Java", "Golang", "Javascript"),
                new Person("Alex", 47, 32_0_5__2740)
        );
        return response;
    }

    @GetMapping("/view")
    public Person view(){
        Person person = new Person("John Turturo", 54, 1_000_222);
        return person;
    }
}
