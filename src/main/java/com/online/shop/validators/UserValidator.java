package com.online.shop.validators;

import com.online.shop.dtos.UserDto;
import com.online.shop.entities.User;
import com.online.shop.repositories.UserRepository;
import com.online.shop.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import java.util.Optional;
import java.util.regex.Pattern;

@Service
public class UserValidator {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserService userService;

    private boolean isNotValid(String email) {
        String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\." +
                "[a-zA-Z0-9_+&*-]+)*@" +
                "(?:[a-zA-Z0-9-]+\\.)+[a-z" +
                "A-Z]{2,7}$";

        Pattern pat = Pattern.compile(emailRegex);
        // am comentat sub pentru că mai jos în metoda validate() este adresat separat cazul unui email null
        /** if (email == null)
         return true; */
        return !pat.matcher(email).matches();
    }

    public void validate(UserDto userDto, BindingResult bindingResult) {

        if (userDto.getFullName() == null || userDto.getFullName().isBlank()) {
            FieldError fieldError = new FieldError("userDto", "fullName",
                    "You must have a name!.. or a nickname :)");
            bindingResult.addError(fieldError);
        }

        if (userDto.getEmail() == null || userDto.getEmail().isBlank()) {
            FieldError fieldError = new FieldError("userDto", "email",
                    "You must enter an email address!");
            bindingResult.addError(fieldError);
        } else if (isNotValid(userDto.getEmail())) {
            FieldError fieldError = new FieldError("userDto", "email",
                    "You must enter a valid email address!");
            bindingResult.addError(fieldError);
        } else {
            Optional<User> optionalUser = userRepository.findByEmail(userDto.getEmail());
            if (optionalUser.isPresent()) {
                FieldError fieldError = new FieldError("userDto", "email",
                        "Email is already in use!");
                bindingResult.addError(fieldError);
            }
        }

        if (userDto.getPassword() == null || userDto.getPassword().isBlank()) {
            FieldError fieldError = new FieldError("userDto", "password",
                    "You must enter a password!");
            bindingResult.addError(fieldError);
        } else if (userDto.getPassword().length() < 8) {
            FieldError fieldError = new FieldError("userDto", "password",
                    "Password must have minimum 8 characters!");
            bindingResult.addError(fieldError);
        } else if (!userDto.getPassword().equals(userDto.getConfirmPassword())) {
            FieldError fieldError = new FieldError("userDto", "confirmPassword",
                    "Passwords must match!");
            bindingResult.addError(fieldError);
        }

        if (userDto.getAddress() == null || userDto.getAddress().isBlank()) {
            FieldError fieldError = new FieldError("userDto", "address",
                    "You must enter your address for shipment!");
            bindingResult.addError(fieldError);
        } else if (userDto.getAddress().length() > 64) {
            FieldError fieldError = new FieldError("userDto", "address",
                    "Address must contain maximum 64 characters!");
            bindingResult.addError(fieldError);
        }

        String stringUtils = "dfdfd";
        stringUtils.isBlank();

    }
}
