package com.online.shop.validators;

import com.online.shop.dtos.ProductDto;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

@Service
public class ProductValidator {
    public void validate(ProductDto productDto, BindingResult bindingResult) {
        try {
            Double price = Double.valueOf(productDto.getPrice());
            if (price <= 0) {
                FieldError fieldError = new FieldError("productDto", "price",
                        "Product price must be positive!");
                bindingResult.addError(fieldError);
            }
        } catch (NumberFormatException exception) {
            FieldError fieldError = new FieldError("productDto", "price",
                    "Product price is not a number!");
            bindingResult.addError(fieldError);
        }

        try {
            Integer quantity = Integer.valueOf(productDto.getQuantity());
            if (quantity < 0) {
                FieldError fieldError = new FieldError("productDto", "quantity",
                        "Product quantity in stock cannot be negative!");
                bindingResult.addError(fieldError);
            }
        } catch (NumberFormatException exception) {
            FieldError fieldError = new FieldError("productDto", "quantity",
                    "Product quantity is not an integer!");
            bindingResult.addError(fieldError);
        }

    }
}
