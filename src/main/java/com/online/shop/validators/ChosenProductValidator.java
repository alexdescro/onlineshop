package com.online.shop.validators;

import com.online.shop.dtos.ChosenProductDto;
import com.online.shop.entities.Product;
import com.online.shop.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import java.util.Optional;

@Service
public class ChosenProductValidator {

    @Autowired
    private ProductRepository productRepository;

    public void validate(ChosenProductDto chosenProductDto, String productId, BindingResult bindingResult) {
        try {
            Integer quantity = Integer.valueOf(chosenProductDto.getQuantity());
            Optional<Product> optionalProduct = productRepository.findById(Integer.valueOf(productId));
            if (quantity <= 0) {
                FieldError fieldError = new FieldError("chosenProductDto", "quantity",
                        "Product quantity must be positive!");
                bindingResult.addError(fieldError);
            } else if (quantity > optionalProduct.get().getQuantity()) {
                FieldError fieldError = new FieldError("chosenProductDto", "quantity",
                        "Requested quantity not in stock!");
                bindingResult.addError(fieldError);
            }
        } catch (NumberFormatException exception) {
            FieldError fieldError = new FieldError("chosenProductDto", "quantity",
                    "Product quantity is not a number!");
            bindingResult.addError(fieldError);
        }
    }

    public void validateStockAdjustment(ChosenProductDto chosenProductDto, String productId, BindingResult bindingResult) {
        try {
            Integer quantity = Integer.valueOf(chosenProductDto.getQuantity());
            Optional<Product> optionalProduct = productRepository.findById(Integer.valueOf(productId));
            if (Math.negateExact(quantity) > optionalProduct.get().getQuantity()) {
                FieldError fieldError = new FieldError("chosenProductDto", "quantity",
                        "It is impossible to reduce the stock with more than it is available!");
                bindingResult.addError(fieldError);
            }
        } catch (NumberFormatException exception) {
            FieldError fieldError = new FieldError("chosenProductDto", "quantity",
                    "Product quantity is not a number!");
            bindingResult.addError(fieldError);
        }
    }

    public void validatePriceAdjustment(ChosenProductDto chosenProductDto, String productId, BindingResult bindingResult) {
        try {
            Double price = Double.valueOf(chosenProductDto.getPrice());
            if (price<=0) {
                FieldError fieldError = new FieldError("chosenProductDto", "price",
                        "Price must be positive!");
                bindingResult.addError(fieldError);
            }
        } catch (NumberFormatException exception) {
            FieldError fieldError = new FieldError("chosenProductDto", "price",
                    "The new desired price is not a number!");
            bindingResult.addError(fieldError);
        }
    }
}
