package com.online.shop.services;

import com.online.shop.dtos.ChosenProductDto;
import com.online.shop.dtos.ShoppingCartDto;
import com.online.shop.dtos.ShoppingCartItemDto;
import com.online.shop.entities.ChosenProduct;
import com.online.shop.entities.Product;
import com.online.shop.entities.ShoppingCart;
import com.online.shop.entities.User;
import com.online.shop.repositories.ChosenProductRepository;
import com.online.shop.repositories.ProductRepository;
import com.online.shop.repositories.ShoppingCartRepository;
import com.online.shop.repositories.UserRepository;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ShoppingCartService {

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ChosenProductRepository chosenProductRepository;

    @Autowired
    private ShoppingCartRepository shoppingCartRepository;

    public void addToCart(ChosenProductDto chosenProductDto, String productId, String loggedInUserEmail) {
        Optional<User> optionalUser = userRepository.findByEmail(loggedInUserEmail);
        ShoppingCart shoppingCart = optionalUser.get().getShoppingCart();
        Optional<ChosenProduct> optionalChosenProduct = chosenProductRepository
                .findByProductIdAndShoppingCartId(Integer.valueOf(productId), shoppingCart.getId());

        if (optionalChosenProduct.isPresent()) {
            ChosenProduct chosenProduct = optionalChosenProduct.get();
            chosenProduct.setChosenQuantity(chosenProduct.getChosenQuantity() +
                    Integer.parseInt(chosenProductDto.getQuantity()));
            chosenProductRepository.save(chosenProduct);
        } else {
            buildChosenProduct(chosenProductDto, productId, loggedInUserEmail);
        }
    }

    private void buildChosenProduct(ChosenProductDto chosenProductDto, String productId, String loggedInUserEmail) {
        ChosenProduct chosenProduct = new ChosenProduct();
        chosenProduct.setChosenQuantity(Integer.valueOf(chosenProductDto.getQuantity()));
        Optional<Product> optionalProduct = productRepository.findById(Integer.valueOf(productId));
        chosenProduct.setProduct(optionalProduct.get());
        Optional<User> optionalUser = userRepository.findByEmail(loggedInUserEmail);
        chosenProduct.setShoppingCart(optionalUser.get().getShoppingCart());
        chosenProductRepository.save(chosenProduct);
    }

    public ShoppingCartDto getShoppingCartDtoByUserEmail(String loggedInUserEmail) {
        ShoppingCart shoppingCart = shoppingCartRepository.findByUserEmail(loggedInUserEmail);
        ShoppingCartDto shoppingCartDto = new ShoppingCartDto();

        double subTotal = 0;
        int totalNumberOfProductsInCart = 0;
        for (ChosenProduct chosenProduct : shoppingCart.getChosenProducts()) {
            ShoppingCartItemDto shoppingCartItemDto = new ShoppingCartItemDto();
            shoppingCartItemDto.setName(chosenProduct.getProduct().getName());
            shoppingCartItemDto.setQuantity(String.valueOf(chosenProduct.getChosenQuantity()));
            shoppingCartItemDto.setPrice(String.valueOf(chosenProduct.getProduct().getPrice()));
            shoppingCartItemDto.setId(String.valueOf(chosenProduct.getId()));
            shoppingCartItemDto.setProductId(String.valueOf(chosenProduct.getProduct().getId()));
            double itemSubtotalPrice = chosenProduct.getChosenQuantity() * chosenProduct.getProduct().getPrice();
            shoppingCartItemDto.setTotal(String.valueOf(itemSubtotalPrice));
            subTotal = subTotal + itemSubtotalPrice;
            totalNumberOfProductsInCart += chosenProduct.getChosenQuantity();
            shoppingCartItemDto.setImage(Base64.encodeBase64String(chosenProduct.getProduct().getImage()));

            shoppingCartDto.add(shoppingCartItemDto);
        }
        shoppingCartDto.setSubTotal(String.valueOf(subTotal));
        shoppingCartDto.setTotal(String.valueOf(subTotal + 50));
        shoppingCartDto.setTotalNumberOfProducts(String.valueOf(totalNumberOfProductsInCart));
        return shoppingCartDto;
    }

    public void removeShoppingCartItem(String cartItemId) {
        boolean existsById = chosenProductRepository.existsById(Integer.valueOf(cartItemId));
        if (existsById) {
            chosenProductRepository.deleteById(Integer.valueOf(cartItemId));
        }
    }

    public void increaseItemQtyByOne(String cartItemId) {
        boolean existsById = chosenProductRepository.existsById(Integer.valueOf(cartItemId));
        if (existsById) {
            ChosenProduct chosenProduct = chosenProductRepository.getReferenceById(Integer.valueOf(cartItemId));
            Integer chosenQuantity = chosenProduct.getChosenQuantity();
            Integer newChosenQuantity = chosenQuantity + 1;
            if (newChosenQuantity <= chosenProduct.getProduct().getQuantity()) {
                chosenProduct.setChosenQuantity(newChosenQuantity);
                chosenProductRepository.save(chosenProduct);
            }
        }
    }

    public void decreaseItemQtyByOne(String cartItemId) {
        boolean existsById = chosenProductRepository.existsById(Integer.valueOf(cartItemId));
        if (existsById) {
            ChosenProduct chosenProduct = chosenProductRepository.getReferenceById(Integer.valueOf(cartItemId));
            Integer chosenQuantity = chosenProduct.getChosenQuantity();
            Integer newChosenQuantity = chosenQuantity - 1;
            if (newChosenQuantity > 0) {
                chosenProduct.setChosenQuantity(newChosenQuantity);
                chosenProductRepository.save(chosenProduct);
            }
        }
    }
}
