package com.online.shop.services;

import com.online.shop.dtos.UserDetailsDto;
import com.online.shop.dtos.UserDto;
import com.online.shop.entities.User;
import com.online.shop.mappers.UserMapper;
import com.online.shop.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserMapper userMapper;

    public void addUser(UserDto userDto) {
        User user = userMapper.map(userDto);
        userRepository.save(user);
    }

    public UserDetailsDto getUserDetailsDtoByEmail(String email){
        Optional<User> optionalUser = userRepository.findByEmail(email);
        User user = optionalUser.get();

        UserDetailsDto userDetailsDto = new UserDetailsDto();
        userDetailsDto.setAddress(user.getAddress());
        userDetailsDto.setFullName(user.getFullName());
        return userDetailsDto;
    }


    public UserDto getUserDtoByEmail(String email) {
        Optional<User> optionalUser = userRepository.findByEmail(email);
        User user = optionalUser.get();

        UserDto userDto = new UserDto();
        userDto.setUserRole(String.valueOf(user.getUserRole()));
        userDto.setEmail(user.getEmail());
        userDto.setFullName(user.getFullName());
        return userDto;
    }

    public boolean userIsAdmin(){
        String loggedInUserEmail = SecurityContextHolder.getContext().getAuthentication().getName();
        UserDto userDto = getUserDtoByEmail(loggedInUserEmail);
        String userRole = userDto.getUserRole();
        System.out.println(userRole);
        return userRole.equals("ROLE_ADMIN");
    }
}
